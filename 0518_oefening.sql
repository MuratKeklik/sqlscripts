use modernways;
alter table huisdieren add column geluid varchar(20);
SET SQL_SAFE_UPDATES = 0;
update huisdieren  set geluid = 'waf'where soort = 'hond';
update huisdieren set geluid = 'miaauww' where soort = 'kat';
SET SQL_SAFE_UPDATES = 1;

select geluid, soort
from huisdieren
where soort = 'hond';